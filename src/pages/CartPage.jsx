import React, { useState, useContext } from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";

import "./CartPage.scss";

import Pages, { CardsWrap } from "./components";
import { CardToCart, CardToCartLine } from "../components/Cards";
import ButtonClassic, { ButtonPlateSwitcher } from "../components/Buttons";
import { ModalFormToOrder } from "../components/Modals";

import { toggelModal } from "../store/modal_slices";
import { selectorIsModalForm, selectorCarts } from "../store/selectors";
import { GridCard, GridLine } from "../assets";

import { StoreContext } from "../context/context";

const CartPage = (props) => {
  const { handleModalDelCart, onCurrentProduct, handleFavorite } = props;
  const { isGridSwitcher, showGridCart, showGridLine } =
    useContext(StoreContext);

  const dispatch = useDispatch();
  const isModalForm = useSelector(selectorIsModalForm);
  const carts = useSelector(selectorCarts);

  const handleModalForm = () => {
    dispatch(toggelModal({ modalName: "isModalForm" }));
  };

  return (
    <Pages>
      <>
        <ModalFormToOrder isOpen={isModalForm} onClose={handleModalForm} />
        <div className="pages__cart wrapper">
          <div className="pages__cart button-wrapper">
            <div className="btn__to-order">
              <ButtonClassic onClick={handleModalForm}>TO ORDER</ButtonClassic>
            </div>
            <div className="cart__switcher-wrap">
              <ButtonPlateSwitcher
                className="btn__switcher"
                onClick={showGridCart}
              >
                <GridCard className="svg__switcher" />
              </ButtonPlateSwitcher>
              <ButtonPlateSwitcher
                className="btn__switcher"
                onClick={showGridLine}
              >
                <GridLine className="svg__switcher" />
              </ButtonPlateSwitcher>
            </div>
          </div>

          {isGridSwitcher ? (
            <CardsWrap>
              {carts.map((product) => {
                return (
                  <CardToCart
                    key={product.vendorCode}
                    product={product}
                    onModalDelCart={() => {
                      handleModalDelCart();
                      onCurrentProduct(product);
                    }}
                    handleFavorite={handleFavorite}
                  />
                );
              })}
            </CardsWrap>
          ) : (
            <div className=".cards-wrap line">
              {carts.map((product) => {
                return (
                  <CardToCartLine
                    key={product.vendorCode}
                    product={product}
                    onModalDelCart={() => {
                      handleModalDelCart();
                      onCurrentProduct(product);
                    }}
                    handleFavorite={handleFavorite}
                  />
                );
              })}
            </div>
          )}
        </div>
      </>
    </Pages>
  );
};

CartPage.propTypes = {
  carts: PropTypes.array,
  onCurrentProduct: PropTypes.func,
  handleModalDelCart: PropTypes.func,
  handleFavorite: PropTypes.func,
};

export default CartPage;
