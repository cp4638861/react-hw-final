import React from "react";
import PropTypes from "prop-types";
import { Field, ErrorMessage, useField } from "formik";
import cn from "classnames";

import "./Input.scss";

const Input = (props) => {
  const {
    className,
    type = "text",
    placeholder,
    name,
    label,
    error,
    ...restProps
  } = props;

  const [field, meta] = useField(name);

  return (
    <label className={cn("label__order", className)}>
      <p className="label__title">{label}</p>
      <Field
        className={cn("input__order", {
          "error-input": meta.touched && meta.error,
        })}
        type={type}
        placeholder={placeholder}
        name={name}
        {...restProps}
      />
      <ErrorMessage name={name} className={"error-message"} component={"p"} />
    </label>
  );
};

Input.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  error: PropTypes.bool,
  restProps: PropTypes.object,
};

export default Input;
