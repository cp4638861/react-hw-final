export const formInitialValues = {
  firstname: "",
  lastname: "",
  age: "",
  country: "",
  company: "",
  address: "",
  apartment: "",
  city: "",
  state: "",
  postcode: "",
  phone: "",
};

// export const formInitialValues = {
//   firstname: "Andrii",
//   lastname: "Kucher",
//   age: "38",
//   country: "Ukraine",
//   company: "Raynor-co",
//   address: "New st.",
//   apartment: "4",
//   city: "Kyiv",
//   state: "Kyiv oblast",
//   postcode: "010010",
//   phone: "+38 (606) 06-60-6-66",
// };
