import React, { useState } from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./CardToCartLine.scss";

import { ButtonCornerDelete, ButtonCornerHeart } from "../Buttons";
import { CardElement } from "./";

const CardToCartLine = (props) => {
  const { className, product, onModalDelCart, handleFavorite } = props;
  const [isFavorite, setIsFavorite] = useState(product.addedToFavorite);

  const toggleFavorite = () => {
    handleFavorite(product);
    setIsFavorite(!isFavorite);
  };

  return (
    <div className={cn("card-line", className)}>
      <div className={cn("card__content-line", className)}>
        <div className={cn("card__img-box", className)}>
          <img src={product.path} alt={product.name} className="card__img" />
        </div>
        <div className="card__main-line">
          <p className="product-name">{product.name}</p>
          <p className="product-quantity">Quantity: {product.basketCounter}</p>
          <p className="product-article">Article: {product.vendorCode}</p>
        </div>
      </div>
      <ButtonCornerHeart
        className="btn__corner_right-extra"
        onClick={toggleFavorite}
        isActive={isFavorite}
      />

      <ButtonCornerDelete onClick={onModalDelCart} />

      <CardElement className="position_right-top">
        <div
          className="card__color"
          style={{ backgroundColor: product.colorCode }}
        ></div>
      </CardElement>
      <p className="card__price">${product.price}</p>
    </div>
  );
};

CardToCartLine.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object,
  handleModalDelCart: PropTypes.func,
  onModalDelCart: PropTypes.func,
  handleFavorite: PropTypes.func,
};

export default CardToCartLine;
