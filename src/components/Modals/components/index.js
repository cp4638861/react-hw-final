import ModalWrapper from "./ModalWrapper";
import ModalBox from "./ModalBox";
import ModalContainer from "./ModalContainer";
import ModalHeader from "./ModalHeader";
import ModalMain from "./ModalMain";
import ModalFooter from "./ModalFooter";

export default ModalWrapper;
export { ModalBox, ModalContainer, ModalHeader, ModalMain, ModalFooter };
