import React from "react";
import PropTypes from "prop-types"

import "./ModalBase.scss";

const ModalContainer = ({ children }) => {
  return <div className="modal__container">{children}</div>;
};

ModalContainer.propTypes = {
  children: PropTypes.any,
}

export default ModalContainer;
