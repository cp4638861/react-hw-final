import ButtonClassic from "./ButtonClassic/ButtonClassic.jsx";
import ButtonCorner from "./ButtonCorner/ButtonCorner.jsx";
import ButtonCornerClose from "./ButtonCorner/ButtonCornerClose.jsx";
import ButtonCornerDelete from "./ButtonCorner/ButtonCornerDelete.jsx";
import ButtonCornerHeart from "./ButtonCorner/ButtonCornerHeart.jsx";
import ButtonCornerCart from "./ButtonCorner/ButtonCornerCart.jsx";
import ButtonPlate from "./ButtonPlate/ButtonPlate.jsx";
import ButtonPlateSwitcher from "./ButtonPlate/ButtonPlateSwitcher.jsx";

export default ButtonClassic;
export {
  ButtonCorner,
  ButtonCornerClose,
  ButtonCornerDelete,
  ButtonCornerHeart,
  ButtonCornerCart,
  ButtonPlate,
  ButtonPlateSwitcher,
};
