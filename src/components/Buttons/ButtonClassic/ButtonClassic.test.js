import React from "react";
import { render, screen } from "@testing-library/react";
import ButtonClassic from "./ButtonClassic";

describe("ButtonClassic test", () => {
  test("should chilren text", () => {
    const buttonClassic = render(<ButtonClassic>asfsadf testadf</ButtonClassic>);
    expect(buttonClassic.getByText(/test/i)).toBeInTheDocument();
  });
});
