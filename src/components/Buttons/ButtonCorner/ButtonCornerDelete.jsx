import React from "react";
import PropTypes from "prop-types";

import ButtonCorner from "./ButtonCorner";
import Delete from "../icons/delete.svg?react";

const ButtonCornerDelete = ({ onClick }) => {
  return (
    <ButtonCorner className="btn__corner-right" onClick={onClick}>
      <Delete className="svg svg--delete" />
    </ButtonCorner>
  );
};

ButtonCornerDelete.propTypes = {
  onClick: PropTypes.func,
};

export default ButtonCornerDelete;
