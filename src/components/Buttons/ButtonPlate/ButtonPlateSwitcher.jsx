import React from "react";
import PropTypes from "prop-types";

import "./ButtonPlateSwitcher.scss";

const ButtonPlateSwitcher = (props) => {
  const { className, type = "button", onClick, children, ...restProps } = props;

  return (
    <button className={className} type={type} onClick={onClick} {...restProps}>
      {children}
    </button>
  );
};

ButtonPlateSwitcher.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.any,
  restProps: PropTypes.object,
};

export default ButtonPlateSwitcher;
