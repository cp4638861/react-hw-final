import React, { useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import "./App.scss";

import Header from "./components/Header";
import Footer from "./components/Footer/Footer.jsx";
import {
  ModalAddProduct,
  ModalDelCart,
  ModalDelFavorite,
} from "./components/Modals";

import { HomePage, CartPage, FavoritePage, NotFoundPage } from "./pages";

import {
  actionFetchAsyncThunk,
  setProducts,
  setCurrentProduct,
  setToCart,
  setFavorite,
} from "./store/rootSlice.js";

import {
  selectorError,
  selectorProducts,
  selectorCurrentProduct,
  selectorCarts,
  selectorFavorites,
  selectorIsModalProduct,
  selectorIsModalDelCart,
  selectorIsModalDelFavorite,
} from "./store/selectors.js";

import { toggelModal } from "./store/modal_slices.js";

const App = () => {
  const dispatch = useDispatch();

  const error = useSelector(selectorError);
  const products = useSelector(selectorProducts);
  const currentProduct = useSelector(selectorCurrentProduct);
  const carts = useSelector(selectorCarts);
  const favorites = useSelector(selectorFavorites);

  const isModalProduct = useSelector(selectorIsModalProduct);
  const isModalDelCart = useSelector(selectorIsModalDelCart);
  const isModalDelFavorite = useSelector(selectorIsModalDelFavorite);

  useEffect(() => {
    dispatch(actionFetchAsyncThunk());
  }, [dispatch]);

  const handleToCart = (item) => {
    const updatedProducts = products.map((product) =>
      product.vendorCode === item.vendorCode
        ? {
            ...product,
            addedToCart: true,
            basketCounter: product.basketCounter + 1,
          }
        : product
    );
    dispatch(setProducts(updatedProducts));
    localStorage.setItem("products", JSON.stringify(updatedProducts));

    const existingProductIndex = carts.findIndex(
      (cart) => cart.vendorCode === item.vendorCode
    );

    let newState;
    if (existingProductIndex >= 0) {
      newState = carts.map((cart, index) =>
        index === existingProductIndex
          ? {
              ...cart,
              addedToCart: true,
              basketCounter: cart.basketCounter + 1,
            }
          : cart
      );
    } else {
      newState = [...carts, { ...item, addedToCart: true, basketCounter: 1 }];
    }
    dispatch(setToCart(newState));

    localStorage.setItem("carts", JSON.stringify(newState));
  };

  const removeCart = (item) => {
    const updatedProducts = products.map((product) =>
      product.vendorCode === item.vendorCode
        ? { ...product, addedToCart: false, basketCounter: 0 }
        : product
    );
    dispatch(setProducts(updatedProducts));
    localStorage.setItem("products", JSON.stringify(updatedProducts));

    const updatedCarts = carts.filter(
      (cartItem) => cartItem.vendorCode !== item.vendorCode
    );
    dispatch(setToCart(updatedCarts));
    localStorage.setItem("carts", JSON.stringify(updatedCarts));

    const updatedFavorites = favorites.map((favoriteItem) =>
      favoriteItem.vendorCode === item.vendorCode
        ? { ...favoriteItem, addedToCart: false, basketCounter: 0 }
        : favoriteItem
    );
    dispatch(setFavorite(updatedFavorites));
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };

  const handleFavorite = (item) => {
    const updatedProducts = products.map((product) =>
      product.vendorCode === item.vendorCode
        ? { ...product, addedToFavorite: !item.addedToFavorite }
        : product
    );
    dispatch(setProducts(updatedProducts));
    localStorage.setItem("products", JSON.stringify(updatedProducts));

    const updatedCarts = carts.map((cartItem) =>
      cartItem.vendorCode === item.vendorCode
        ? { ...cartItem, addedToFavorite: !item.addedToFavorite }
        : cartItem
    );
    dispatch(setToCart(updatedCarts));
    localStorage.setItem("carts", JSON.stringify(updatedCarts));

    if (item.addedToFavorite) {
      const updatedFavorites = favorites.filter(
        (fav) => fav.vendorCode !== item.vendorCode
      );
      dispatch(setFavorite(updatedFavorites));
      localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
    } else {
      dispatch(setFavorite([...favorites, item]));
      localStorage.setItem("favorites", JSON.stringify([...favorites, item]));
    }
  };

  const removeFavorite = (item) => {
    const updatedProducts = products.map((product) =>
      product.vendorCode === item.vendorCode
        ? { ...product, addedToFavorite: false }
        : product
    );
    dispatch(setProducts(updatedProducts));
    localStorage.setItem("products", JSON.stringify(updatedProducts));

    const updatedFavorites = favorites.filter(
      (fav) => fav.vendorCode !== item.vendorCode
    );
    dispatch(setFavorite(updatedFavorites));
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));

    const updatedCarts = carts.map((cartItem) =>
      cartItem.vendorCode === item.vendorCode
        ? { ...cartItem, addedToFavorite: false }
        : cartItem
    );
    dispatch(setToCart(updatedCarts));
    localStorage.setItem("carts", JSON.stringify(updatedCarts));
  };

  const handlCurrentProduct = (product) => {
    dispatch(setCurrentProduct(product));
  };

  const handleModalProduct = () => {
    dispatch(toggelModal({ modalName: "isModalProduct" }));
  };

  const handleModalDelCart = () => {
    dispatch(toggelModal({ modalName: "isModalDelCart" }));
  };

  const handleModalDelFavorite = () => {
    dispatch(toggelModal({ modalName: "isModalDelFavorite" }));
  };

  return (
    <div className="container">
      <Header countCarts={carts.length} countFavorites={favorites.length} />

      <main className="inner main">
        {error && (
          <div className="error wrapper">
            <p className="error code">ERROOORR!!</p>
            <p className="error text">
              Something in FetchAsyncThunk had gone wrong
            </p>
            <p>Message error: {error}</p>
          </div>
        )}
        <Routes>
          <Route
            path="/"
            element={
              <HomePage
                products={products}
                handleModalProduct={handleModalProduct}
                onCurrentProduct={handlCurrentProduct}
                handleFavorite={handleFavorite}
              />
            }
          />
          <Route
            path="/carts"
            element={
              <CartPage
                handleModalDelCart={handleModalDelCart}
                onCurrentProduct={handlCurrentProduct}
                handleFavorite={handleFavorite}
              />
            }
          />
          <Route
            path="/favorites"
            element={
              <FavoritePage
                isOpenModalPruduct={handleModalProduct}
                onCurrentProduct={handlCurrentProduct}
                handleModalDelFavorite={handleModalDelFavorite}
              />
            }
          />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </main>

      <Footer className="footer">React HomeWork | Kucher Andrii | 2024</Footer>

      <ModalAddProduct
        isOpen={isModalProduct}
        onClose={handleModalProduct}
        currentProduct={currentProduct}
        handleModalProduct={handleModalProduct}
        onAddToCart={() => {
          handleToCart(currentProduct);
        }}
      />

      <ModalDelCart
        isOpen={isModalDelCart}
        onClose={handleModalDelCart}
        currentProduct={currentProduct}
        removeCart={removeCart}
      />

      <ModalDelFavorite
        isOpen={isModalDelFavorite}
        onClose={handleModalDelFavorite}
        currentProduct={currentProduct}
        removeFavorite={removeFavorite}
      />
    </div>
  );
};

export default App;
